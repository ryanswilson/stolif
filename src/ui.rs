use conrod_core::position::{Align, Relative};
use conrod_core::widget_ids;
use conrod_core::{
    color, widget, Color, Colorable, Labelable, Positionable, Sizeable,
    Widget,
};
use glium::Surface;
use glium::{
    glutin::{event, event_loop},
    Display,
};
use nfd::Response;
use rand::prelude::ThreadRng;
use serde::{Deserialize, Serialize};
use std::io::Write;

pub const WIN_W: u32 = 600;
pub const WIN_H: u32 = 420;

use crate::stolif::Stolif;
// use crossbeam_channel::unbounded;
use rand::thread_rng;
use std::sync::{Arc, Mutex};

// ui code cromes from https://birdink.gitlab.io/conrod/

pub fn ui_code(stolif_lock: Arc<Mutex<Stolif>>) -> () {
    // let (s_stol, r_stol) = unbounded();
    let loop_stolif = stolif_lock.clone();
    let rng = thread_rng();
    let mut stol_com = StolifComm {
        stolif_lock: loop_stolif,
        rng,
    };

    let events_loop = glium::glutin::event_loop::EventLoop::new();
    let window = glium::glutin::window::WindowBuilder::new()
        .with_title("Hello, world!")
        .with_inner_size(glium::glutin::dpi::LogicalSize::new(
            WIN_W, WIN_H,
        ));

    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);

    let display =
        glium::Display::new(window, context, &events_loop).unwrap();

    // Construct our `Ui`.
    let mut ui =
        conrod_core::UiBuilder::new([WIN_W as f64, WIN_H as f64])
            // .theme(conrod_example_shared::theme())
            .build();

    let font_path =
        "/home/ryan/Desktop/work/stolif/assets/IMFePIrm29P.ttf";
    ui.fonts.insert_from_file(font_path).unwrap();
    // let display = GliumDisplayWinitWrapper(display);

    // TODO: put widgets here
    let mut renderer = conrod_glium::Renderer::new(&display).unwrap();
    // let image_map =
    //     conrod_core::image::Map::<glium::texture::Texture2d>::new();
    let mut image_map = conrod_core::image::Map::new();

    let stolim =
        image_map.insert(stol_com.texture_from_stolif(&display));

    let mut running = false;
    let mut run_num: String = "0".to_string();

    let mut ids = Ids::new(ui.widget_id_generator());
    ids.ps.resize(9, &mut ui.widget_id_generator());

    run_loop(display, events_loop, move |request, display| {
        match request {
            Request::SetUi { needs_redraw } => {
                let ui = &mut ui.set_widgets();

                widget::Canvas::new()
                    .pad(20.0)
                    .color(color::rgb(0.05, 0.05, 0.5))
                    .set(ids.canvas, ui);

                {
                    let mut stolif =
                        stol_com.stolif_lock.lock().unwrap();
                    for i in 0..9 {
                        for new_p in widget::Slider::new(
                            v_to_p(&stolif.p_lifes[i]),
                            0.0,
                            1.0,
                        )
                        .w_h(100.0, 30.0)
                        .label(&format!("{}", i))
                        .x_y(150.0, 120.0 - (i as f64) * 35.0)
                        .set(ids.ps[i], ui)
                        {
                            stolif.p_lifes[i] = p_to_v(new_p);
                        }
                    }
                }

                for _click in widget::Button::new()
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .top_right_of(ids.canvas)
                    .w_h(80.0, 30.0)
                    .label("step".into())
                    .set(ids.step, ui)
                {
                    stol_com.step();
                    image_map.replace(
                        stolim,
                        stol_com.texture_from_stolif(&display),
                    );
                }

                for _click in widget::Button::new()
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .x_position_relative(Relative::Align(Align::End))
                    .w_h(80.0, 30.0)
                    .label("clear".into())
                    .set(ids.clear, ui)
                {
                    stol_com.clear();
                    image_map.replace(
                        stolim,
                        stol_com.texture_from_stolif(&display),
                    );
                }

                for _click in widget::Button::new()
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .x_position_relative(Relative::Align(Align::End))
                    .w_h(80.0, 30.0)
                    .label("Play".into())
                    .set(ids.start, ui)
                {
                    running = !running;
                }

                for _click in widget::Button::new()
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .x_position_relative(Relative::Align(Align::End))
                    .w_h(80.0, 30.0)
                    .label("Import".into())
                    .set(ids.import, ui)
                {
                    let result = nfd::open_file_dialog(None, None)
                        .unwrap_or_else(|e| {
                            panic!(e);
                        });

                    match result {
                        Response::Okay(file_path) => {
                            stol_com.load_settings(file_path)
                        }
                        Response::OkayMultiple(mut files) => match files
                            .pop()
                        {
                            Some(file) => stol_com.load_settings(file),
                            None => {}
                        },
                        Response::Cancel => {}
                    }
                }

                for _click in widget::Button::new()
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .x_position_relative(Relative::Align(Align::End))
                    .w_h(80.0, 30.0)
                    .label("Export".into())
                    .set(ids.export, ui)
                {
                    let result = nfd::open_save_dialog(None, None)
                        .unwrap_or_else(|e| {
                            panic!(e);
                        });

                    match result {
                        Response::Okay(file_path) => {
                            stol_com.export_settings(file_path)
                        }
                        Response::OkayMultiple(mut files) => match files
                            .pop()
                        {
                            Some(file) => stol_com.export_settings(file),
                            None => {}
                        },
                        Response::Cancel => {}
                    }
                }

                for event in widget::TextBox::new(&run_num)
                    .font_size(20)
                    .w_h(80.0, 30.0)
                    .set(ids.runn, ui)
                {
                    match event {
                        widget::text_box::Event::Enter => {}
                        widget::text_box::Event::Update(string) => {
                            match string.parse::<u32>() {
                                Ok(_) => run_num = string,
                                Err(_) => {}
                            }
                        }
                    }
                }

                for _event in widget::Button::new()
                    .w_h(80.0, 30.0)
                    .press_color(Color::Rgba(0.5, 0.5, 0.5, 0.0))
                    .label("Import".into())
                    .set(ids.save_run, ui)
                {
                    // TODO: multi thread job/viewer
                    let dest = nfd::open_save_dialog(None, None)
                        .unwrap_or_else(|e| {
                            panic!(e);
                        });

                    let path = match dest {
                        Response::Okay(file_path) => file_path,
                        Response::OkayMultiple(mut files) => {
                            match files.pop() {
                                Some(file) => file,
                                None => break,
                            }
                        }
                        Response::Cancel => break,
                    };

                    match run_num.parse::<u32>() {
                        Ok(n) => stol_com.save_run(path, n),
                        Err(_) => {}
                    }
                }

                widget::Image::new(stolim)
                    .w_h(300.0, 300.0)
                    .top_left_of(ids.canvas)
                    .set(ids.stolim, ui);

                *needs_redraw = ui.has_changed();
            }

            Request::Tick { needs_redraw } => {
                if running {
                    stol_com.step();
                    image_map.replace(
                        stolim,
                        stol_com.texture_from_stolif(&display),
                    );
                    *needs_redraw = true;
                }
            }

            Request::Redraw => {
                // Render the `Ui` and then display it on the screen.
                let primitives = ui.draw();

                renderer.fill(display, primitives, &image_map);
                let mut target = display.draw();
                target.clear_color(0.0, 0.0, 1.0, 1.0);
                renderer.draw(display, &mut target, &image_map).unwrap();
                target.finish().unwrap();
            }
            Request::Event {
                event,
                should_update_ui,
                should_exit,
            } => {
                // Use the `winit` backend feature to convert the winit event to a conrod one.
                if let Some(event) =
                    convert_event(&event, &display.gl_window().window())
                {
                    ui.handle_event(event);
                    *should_update_ui = true;
                }

                match event {
                    glium::glutin::event::Event::WindowEvent { event, .. } => match event {
                        // Break from the loop upon `Escape`.
                        glium::glutin::event::WindowEvent::CloseRequested
                        | glium::glutin::event::WindowEvent::KeyboardInput {
                            input:
                                glium::glutin::event::KeyboardInput {
                                    virtual_keycode:
                                        Some(glium::glutin::event::VirtualKeyCode::Escape),
                                    ..
                                },
                            ..
                        } => *should_exit = true,
                        _ => {}
                    },
                    _ => {}
                }
            }
        }
    });
}

widget_ids! (struct Ids{
    text, canvas, step,
    stolim, clear, ps[],
    start, import, save_run,
    export, runn
});

conrod_winit::v023_conversion_fns!();

pub enum Request<'a, 'b: 'a> {
    Event {
        event: &'a event::Event<'b, ()>,
        should_update_ui: &'a mut bool,
        should_exit: &'a mut bool,
    },
    SetUi {
        needs_redraw: &'a mut bool,
    },
    Redraw,
    Tick {
        needs_redraw: &'a mut bool,
    },
}

#[derive(Debug, Deserialize, Serialize)]
struct StolSettings {
    ps: [f32; 9],
}

#[derive(Debug, Serialize)]
struct StolRun {
    frames: Vec<Vec<Vec<u8>>>,
}

struct StolifComm {
    stolif_lock: Arc<Mutex<Stolif>>,
    rng: ThreadRng,
}

impl StolifComm {
    fn step(&mut self) -> () {
        let mut stolif = self.stolif_lock.lock().unwrap();
        stolif.step(&mut self.rng);
    }

    fn load_settings(&mut self, path: String) -> () {
        let set_json = std::fs::read_to_string(path).expect("nerrr");
        let ps: StolSettings =
            serde_json::from_str(&set_json).expect("Bad_Settings");
        let mut stolif = self.stolif_lock.lock().unwrap();
        for i in 0..9 {
            stolif.p_lifes[i] = p_to_v(ps.ps[i]);
        }
    }

    fn export_settings(&self, path: String) -> () {
        let stolif = self.stolif_lock.lock().unwrap();
        let mut parr = [0.0; 9];
        for i in 0..9 {
            parr[i] = v_to_p(&stolif.p_lifes[i]);
        }
        let eset = StolSettings { ps: parr };
        let j = serde_json::to_string(&eset)
            .expect("json generation should not fail");
        let mut file =
            std::fs::File::create(path).expect("File creation failed");
        file.write_all(j.as_bytes()).expect("Settings write failed");
    }

    fn save_run(&mut self, path: String, n: u32) -> () {
        let mut stolif = self.stolif_lock.lock().unwrap();
        let mut file =
            std::fs::File::create(path).expect("File creation failed");
        let run_vec = Vec::new();
        let mut s_run = StolRun { frames: run_vec };
        for _ in 0..n {
            stolif.step(&mut self.rng);
            s_run.frames.push(stolif.array.clone());
            // let os = frame_to_string(&stolif.array);
            // file.write(os.as_bytes());
        }
        let mut obuf = Vec::new();
        s_run
            .serialize(&mut rmp_serde::Serializer::new(&mut obuf))
            .unwrap();
        file.write(&obuf).expect("file write failed");
        file.flush().expect("file flush failed");
    }

    fn clear(&mut self) -> () {
        let mut stolif = self.stolif_lock.lock().unwrap();
        stolif.array = vec![vec![0; stolif.n]; stolif.n];
    }

    fn texture_from_stolif(
        &self,
        display: &glium::Display,
    ) -> glium::texture::Texture2d {
        let stolif = self.stolif_lock.lock().unwrap();
        let dims = (stolif.n as u32, stolif.n as u32);
        let mut bvec = Vec::new();
        for i in 0..stolif.n {
            for j in 0..stolif.n {
                for _ in 0..3 {
                    bvec.push(stolif.array[i][j] as f32);
                }
            }
        }
        let raw_im =
            glium::texture::RawImage2d::from_raw_rgb(bvec, dims);
        let texture =
            glium::texture::Texture2d::new(display, raw_im).unwrap();
        texture
    }
}

fn frame_to_string(frame: &Vec<Vec<u8>>) -> String {
    let mut ws: String = "".to_string();
    for row in frame {
        for sym in row {
            ws.push_str(&sym.to_string());
        }
        ws.push_str(";");
    }
    ws.push_str("\n");
    return ws;
}

fn v_to_p(v: &crate::stolif::P) -> f32 {
    match v {
        crate::stolif::P::Z => return 0.0,
        crate::stolif::P::One => return 1.0,
        crate::stolif::P::V(p) => return *p,
    }
}

fn p_to_v(p: f32) -> crate::stolif::P {
    match p {
        // TODO: doesn't give anything other than P::V probably
        0.0 => crate::stolif::P::Z,
        1.0 => crate::stolif::P::One,
        _ => crate::stolif::P::V(p),
    }
}

pub fn run_loop<F>(
    display: Display,
    event_loop: event_loop::EventLoop<()>,
    mut callback: F,
) where
    F: 'static + FnMut(Request, &Display),
{
    let sixteen_ms = std::time::Duration::from_millis(16);
    let tick_len = std::time::Duration::from_millis(100);
    let mut next_tick = std::time::Instant::now() + tick_len;
    let mut next_update = None;
    let mut ui_update_needed = false;
    event_loop.run(move |event, _, control_flow| {
        {
            let mut should_update_ui = false;
            let mut should_exit = false;
            callback(
                Request::Event {
                    event: &event,
                    should_update_ui: &mut should_update_ui,
                    should_exit: &mut should_exit,
                },
                &display,
            );
            ui_update_needed |= should_update_ui;
            if should_exit {
                *control_flow = event_loop::ControlFlow::Exit;
                return;
            }
        }

        // We don't want to draw any faster than 60 FPS, so set the UI only on every 16ms, unless:
        // - this is the very first event, or
        // - we didn't request update on the last event and new events have arrived since then.
        let should_set_ui_on_main_events_cleared =
            next_update.is_none() && ui_update_needed;
        match (&event, should_set_ui_on_main_events_cleared) {
            (
                event::Event::NewEvents(event::StartCause::Init {
                    ..
                }),
                _,
            )
            | (
                event::Event::NewEvents(
                    event::StartCause::ResumeTimeReached { .. },
                ),
                _,
            )
            | (event::Event::MainEventsCleared, true) => {
                next_update =
                    Some(std::time::Instant::now() + sixteen_ms);
                ui_update_needed = false;

                let mut needs_redraw = false;
                callback(
                    Request::SetUi {
                        needs_redraw: &mut needs_redraw,
                    },
                    &display,
                );
                if needs_redraw {
                    display.gl_window().window().request_redraw();
                } else {
                    // We don't need to redraw anymore until more events arrives.
                    next_update = None;
                }
            }
            _ => {}
        }

        match next_update {
            Some(next_update) => {
                if next_tick < next_update {
                    *control_flow =
                        event_loop::ControlFlow::WaitUntil(next_tick);
                } else {
                    *control_flow =
                        event_loop::ControlFlow::WaitUntil(next_update);
                }
            }
            None => {
                if std::time::Instant::now() < next_tick {
                    *control_flow =
                        event_loop::ControlFlow::WaitUntil(next_tick);
                } else {
                    // *control_flow = event_loop::ControlFlow::Wait;
                }
            }
        }

        if std::time::Instant::now() < next_tick {
        } else {
            next_tick = std::time::Instant::now() + tick_len;
            let mut needs_redraw = false;
            callback(
                Request::Tick {
                    needs_redraw: &mut needs_redraw,
                },
                &display,
            );
            if needs_redraw {
                display.gl_window().window().request_redraw();
            }
        }

        // Request redraw if needed.
        match &event {
            event::Event::RedrawRequested(_) => {
                callback(Request::Redraw, &display);
            }
            _ => {}
        }
    })
}
