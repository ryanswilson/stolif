use rand::distributions::{Distribution, Uniform};
use rand::rngs::ThreadRng;
use std::vec::Vec;

pub enum P {
    Z,
    One,
    V(f32),
}

pub struct Stolif {
    pub array: Vec<Vec<u8>>,
    pub p_lifes: [P; 9],
    pub n: usize,
}

impl Stolif {
    pub fn new(n: usize) -> Self {
        let array = vec![vec![0; n]; n];

        let p_lifes = [
            P::V(0.01),
            P::V(0.03),
            P::V(0.3),
            P::V(0.8),
            P::V(0.05),
            P::Z,
            P::Z,
            P::Z,
            P::Z,
        ];

        Stolif { array, p_lifes, n }
    }

    pub fn step(&mut self, mut rng: &mut ThreadRng) -> () {
        let mut new = vec![vec![0; self.n]; self.n];
        let dist = Uniform::new(0.0, 1.0);
        for i in 0..self.n {
            for j in 0..self.n {
                let mut acc = 0;
                for di in [-1, 0, 1] {
                    for dj in [-1, 0, 1] {
                        if di == 0 && dj == 0 {
                        } else {
                            acc += self.array[radd(i, di, self.n)]
                                [radd(j, dj, self.n)];
                        }
                    }
                }
                match self.p_lifes[acc as usize] {
                    P::Z => new[i][j] = 0,
                    P::One => new[i][j] = 1,
                    P::V(p) => {
                        if dist.sample(&mut rng) < p {
                            new[i][j] = 1;
                        }
                    }
                }
            }
        }
        self.array = new;
    }
}

fn radd(a: usize, b: i32, n: usize) -> usize {
    let tn = n as i32;
    let w: i32 = a as i32 + b;
    return match w {
        w if w < 0 => (tn + w) as usize,
        w if w > (tn - 1) => (w - tn) as usize,
        w => w as usize,
    };
}
