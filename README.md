<!-- baseURL:https://gitlab.com/entropy-production/stochastic-life -->

Stochastic Life, inspired by Conway's game of life. 
Supports saving/loading configurations and exporting runs.


![Example](/images/example.png "Example")
